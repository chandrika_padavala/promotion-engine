package com.ecommerce.promotion.service;

import com.ecommerce.promotion.dto.Product;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CheckoutServiceTest {

    private CheckoutService checkoutService = new CheckoutService();

    @Test
    public void shouldAddPromotionOnAProductOf3() {
        //Arrange
        List<Product> products = new ArrayList<>();
        Product product = new Product("A");
        products.add(product);
        products.add(product);
        products.add(product);

        //Act
        int expectedPrice = checkoutService.getCheckoutTotal(products);

        //Assert
        assertThat(expectedPrice).isEqualTo(130);
    }

    @Test
    public void shouldCalculateCartForProductA() {
        //Arrange
        List<Product> products = new ArrayList<>();
        Product product = new Product("A");
        products.add(product);
        products.add(product);
        products.add(product);
        products.add(product);

        //Act
        int expectedPrice = checkoutService.getCheckoutTotal(products);

        //Assert
        assertThat(expectedPrice).isEqualTo(180);
    }

    @Test
    public void shouldApplyPromotionOnBProductOf2() {
        //Arrange
        List<Product> products = new ArrayList<>();
        Product product = new Product("B");
        products.add(product);
        products.add(product);

        //Act
        int expectedPrice = checkoutService.getCheckoutTotal(products);

        //Assert
        assertThat(expectedPrice).isEqualTo(45);
    }

    @Test
    public void shouldCalculateCartForProductBOnly() {
        //Arrange
        List<Product> products = new ArrayList<>();
        Product product = new Product("B");
        products.add(product);
        products.add(product);
        products.add(product);

        //Act
        int expectedPrice = checkoutService.getCheckoutTotal(products);

        //Assert
        assertThat(expectedPrice).isEqualTo(75);
    }

    @Test
    public void shouldApplyPromotionOnCAndDProduct() {
        //Arrange
        List<Product> products = new ArrayList<>();
        products.add(new Product("C"));
        products.add(new Product("D"));

        //Act
        int expectedPrice = checkoutService.getCheckoutTotal(products);

        //Assert
        assertThat(expectedPrice).isEqualTo(30);
    }

    @Test
    public void shouldCalculateCartScenarioA() {
        //Arrange
        List<Product> products = new ArrayList<>();
        products.add(new Product("A"));
        products.add(new Product("B"));
        products.add(new Product("C"));

        //Act
        int expectedPrice = checkoutService.getCheckoutTotal(products);

        //Assert
        assertThat(expectedPrice).isEqualTo(100);
    }

    @Test
    public void shouldCalculateCartScenarioB() {
        //Arrange
        List<Product> products = new ArrayList<>();
        products.addAll(Collections.nCopies(5,new Product("A")));
        products.addAll(Collections.nCopies(5,new Product("B")));
        products.add(new Product("C"));

        //Act
        int expectedPrice = checkoutService.getCheckoutTotal(products);

        //Assert
        assertThat(expectedPrice).isEqualTo(370);
    }

    @Test
    public void shouldCalculateCartScenarioC() {
        //Arrange
        List<Product> products = new ArrayList<>();
        products.addAll(Collections.nCopies(3,new Product("A")));
        products.addAll(Collections.nCopies(5,new Product("B")));
        products.add(new Product("C"));
        products.add(new Product("D"));

        //Act
        int expectedPrice = checkoutService.getCheckoutTotal(products);

        //Assert
        assertThat(expectedPrice).isEqualTo(280);
    }

}