import com.ecommerce.promotion.dto.Product;
import com.ecommerce.promotion.service.CheckoutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.Integer.parseInt;

@SpringBootApplication
public class PromotionApplication{
    private static final Logger logger = LoggerFactory.getLogger(PromotionApplication.class);

    public static void main(String args[]) throws IOException {
        logger.info("Application started with command-line arguments: {} ", Arrays.toString(args));
        List<Product> products = new ArrayList<>();

        System.out.println("Please Enter Total Number of orders");
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        int noOfProducts = parseInt(reader.readLine());

        for (int i = 0; i < noOfProducts; i++)
        {
            System.out.println("Enter the type of product:A,B,C or D");
            String type= reader.readLine();
            Product product = new Product(type);
            products.add(product);
        }
        CheckoutService checkoutService = new CheckoutService();
        int totalPrice = checkoutService.getCheckoutTotal(products);
        System.out.println("Total cost of you Cart : "+totalPrice+" Rs");
    }
}
