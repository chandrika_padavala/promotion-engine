package com.ecommerce.promotion.service;

import com.ecommerce.promotion.dto.Product;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ecommerce.promotion.PriceConstants.*;

@Service
public class CheckoutService {

    public int getCheckoutTotal(List<Product> products) {
        int counterA = 0, counterB = 0, counterC = 0, counterD = 0;
        for (Product product : products) {
            switch (product.getType()){
                case "A":
                case "a": counterA++;
                break;
                case "B":
                case "b": counterB++;
                break;
                case "C":
                case "c": counterC++;
                break;
                case "D":
                case "d": counterD++;
            }
        }
        int totalPriceOfA = getTotalPriceOfProduct(counterA, 3, PROMOTION_COST_OF_PRODUCT_A, PRICE_OF_PRODUCT_A);
        int totalPriceOfB = getTotalPriceOfProduct(counterB, 2, PROMOTION_COST_OF_PRODUCT_B, PRICE_OF_PRODUCT_B);
        int totalPriceOfCAndD = getTotalPriceOfCAndD(counterC, counterD);
        return totalPriceOfA + totalPriceOfB + totalPriceOfCAndD;
    }

    private int getTotalPriceOfCAndD(int counterC, int counterD) {
        int totalPriceOfCAndD = 0;
        if (counterC == counterD) {
            totalPriceOfCAndD = counterC * PROMOTION_FOR_C_AND_D;
        } else {
            totalPriceOfCAndD = counterC * PRICE_OF_PRODUCT_C + counterD * PRICE_OF_PRODUCT_D;
        }
        return totalPriceOfCAndD;
    }

    private int getTotalPriceOfProduct(int counterA, int promotionCount, int promotionCostOfProductA, int priceOfProductA) {
        return (counterA / promotionCount) * promotionCostOfProductA + (counterA % promotionCount) * priceOfProductA;
    }
}
