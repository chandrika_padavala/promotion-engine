package com.ecommerce.promotion;

public class PriceConstants {
    public static final int PRICE_OF_PRODUCT_A = 50;
    public static final int PRICE_OF_PRODUCT_B = 30;
    public static final int PRICE_OF_PRODUCT_C = 20;
    public static final int PRICE_OF_PRODUCT_D = 15;

    public static final int PROMOTION_COST_OF_PRODUCT_A = 130;
    public static final int PROMOTION_COST_OF_PRODUCT_B = 45;
    public static final int PROMOTION_FOR_C_AND_D = 30;
}
