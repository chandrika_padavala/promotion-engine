package com.ecommerce.promotion.dto;

import lombok.Data;

@Data
public class Product {
    private String type;
    private long price;

    public Product(String type) {
        this.type = type;
    }
}
